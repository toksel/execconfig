﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Winforms = System.Windows.Forms;
using System.IO;

namespace ExecConfig
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            // загружаемся от имени администратора
            WindowsPrincipal principal = new WindowsPrincipal(WindowsIdentity.GetCurrent());
            if (!principal.IsInRole(WindowsBuiltInRole.Administrator))
            {
                ProcessStartInfo startInfo = new ProcessStartInfo();
                startInfo.Verb = "runas";
                startInfo.FileName = Winforms.Application.ExecutablePath;
                try
                {
                    Process.Start(startInfo);
                }
                catch (System.ComponentModel.Win32Exception)
                {

                }
                this.Close();
            }
            else
            {
                InitializeComponent();
                TextValues.namesDtsConfigList = "";
            }           
        }

        private void btn_start_Button_Click(object sender, RoutedEventArgs e)
        {           
            if (!File.Exists(TextValues.fullPathDtsx))
            {
                string text = (txtbox_dstx_name.Text == "") ? ".dtsx" : TextValues.fullPathDtsx;
                string summaryOutput = $"{text} не обнаружен";
                MessageBox.Show(summaryOutput);
                return;
            }
            else if (string.IsNullOrWhiteSpace(txtbox_bat_name.Text))
            {
                string summaryOutput = $"Недопустимое название .bat файла";
                MessageBox.Show(summaryOutput);
                return;
            }

            // проверяем еще раз на остутствующие конфиги на случай если не выбирали папку расположения конфиг файлов
            //очищаем список отсутсвующих в папке конфигов
            TextValues.namesDtsConfigListMissing = "";

            //папка с конфигами
            string configLocationPath = TextValues.folderDtsx;

            //заполняем список 
            List<string> namesDtsConfigListMissing = new List<string>();

            List<string> namesDtsConfigList = TextValues.namesDtsConfigList.Split(',').ToList();
            foreach (var conf in namesDtsConfigList)
            {
                if (!File.Exists(configLocationPath + "\\" + conf))
                {
                    namesDtsConfigListMissing.Add(conf);
                }
            }

            //проверяем есть ли в списке пропущенных конфигов элементы
            if (namesDtsConfigListMissing.Capacity > 0)
            {
                TextValues.namesDtsConfigListMissing = string.Join(",", namesDtsConfigListMissing);
            }          



            //List<string> namesDtsConfigList = TextValues.namesDtsConfigList.Split(',').ToList();
            // список для конфигов, заполняем его из TextValues в зависимосто от наличия отсутствующих конфигов, т.к. удалить методом Remove из списка не удается
            List<string> namesDtsConfigListLocal = new List<string>();
            //List<string> namesDtsConfigListMissing = TextValues.namesDtsConfigListMissing.Split(',').ToList();


            if (!string.IsNullOrEmpty(TextValues.namesDtsConfigListMissing))
            {

                string warningMessage = "Следующие .dtsConfig не удалось обнаружить в папке " + configLocationPath + $" :\n";
                foreach (var conf in namesDtsConfigListMissing)
                {
                    warningMessage += "- " + conf + "\n";
                }
                warningMessage += "и они будут проигнорированы.";
                Winforms.DialogResult warningDialog = Winforms.MessageBox.Show(warningMessage, "Не все .dtsConfig обнаружены", Winforms.MessageBoxButtons.OKCancel);
                if (warningDialog == Winforms.DialogResult.Cancel)
                {
                    return;
                }
                else
                {
                    foreach (string conf in namesDtsConfigList)
                    {
                        if (!TextValues.namesDtsConfigListMissing.Contains(conf))
                        {
                            namesDtsConfigListLocal.Add(conf);
                        }                        
                    }
                }
            }
            else
            {
                foreach (string conf in namesDtsConfigList)
                {
                    namesDtsConfigListLocal.Add(conf);
                }
            }

            List<string> com = new List<string>();
            //com.Add($"set dirConfig=%cd%");
            com.Add($"cd /d %~dp0");
            com.Add($"cd..");
            com.Add(@"set LogPath=%cd%\LOG");
            com.Add(@"if not exist ""%LogPath%"" mkdir ""%LogPath%""");
            com.Add(@"DTExec ^");
            com.Add(@"/FILE ""%cd%" + TextValues.relatedPath + txtbox_dstx_name.Text + @""" ^");
            foreach (var name in namesDtsConfigListLocal)
            {
                if (!string.IsNullOrWhiteSpace(name))
                {
                    com.Add(@"/CONFIGFILE ""%cd%" + TextValues.relatedPath + name + @""" ^");
                }
            }
            com.Add(@"/MAXCONCURRENT "" -1 "" /CHECKPOINTING OFF > ""%logpath%\log_" + txtbox_dstx_name.Text.Substring(0, txtbox_dstx_name.Text.LastIndexOf('.')) + @"_%date%_%time:~-11,2%-%time:~-8,2%-%time:~-5,2%.txt""");

            //если в пути размещения bat файла есть пробелы, то cmd выдаст ошибку на старте, придется запускать bat вручную
            if (Winforms.Application.StartupPath.ToString().Contains(" "))
            {
                RunCMD(Winforms.Application.StartupPath.ToString(), txtbox_bat_name.Text, com, true);
            }
            else
            {
                RunCMD(Winforms.Application.StartupPath.ToString(), txtbox_bat_name.Text, com, false);
            }

        }

        public static string ReturnPath(string directory, string name)
        {
            string result = "";

            List<string> res = new List<string>();

            var dirs = new Stack<string>();
            dirs.Push(directory);

            while (dirs.Count > 0)
            {
                string currentDirPath = dirs.Pop();

                try
                {
                    string[] subDirs = Directory.GetDirectories(currentDirPath);
                    foreach (var sb in subDirs)
                    {
                        dirs.Push(sb);
                    }

                    string[] allFoundFiles1 = Directory.GetFiles(currentDirPath, name);
                    foreach (var st in allFoundFiles1)
                    {
                        res.Add(st);
                    }
                }
                catch (UnauthorizedAccessException)
                {
                    continue;
                }
                catch (DirectoryNotFoundException)
                {
                    continue;
                }
            }

            foreach (string str in res)
            {
                if (str.Contains(name))
                {
                    result = str;
                }
            }

            return result;
        }
        public static void RunCMD(string path, string name, List<string> commands, bool copyBeforeExec)
        {
            string bat = path + $"\\{name}.bat";

            //Если директории нет, то выход
            if (!Directory.Exists(path))
            {
                return;
            }

            //если БАТник уже есть, то удаляем его
            FileInfo file = new FileInfo(path);
            if (File.Exists(path))
            {
                file.Delete();
            }

            //записываем в батник построчно текст из списка строк и запускаем 
            using (StreamWriter sw = new StreamWriter(bat))
            {
                foreach (var str in commands)
                {
                    sw.WriteLine(str);
                }
            }

            SelectFileUsingExplorer(bat);

        }

        // переопределим диалог выбора папки для задания его начального положения
        public static string SelectFolder(string startupPath)
        {
            string path = "";
            Winforms.FolderBrowserDialog dlg = new Winforms.FolderBrowserDialog(); // using WinForms = System.Windows.Forms; , винформс подключил в референсах
            if (Directory.Exists(startupPath))
            {
                dlg.SelectedPath = startupPath;
            }
            dlg.ShowDialog();
            path = dlg.SelectedPath;
            return path;
        }

        public static void SelectFileUsingExplorer(string path)
        {
            Process pr = new Process();
            ProcessStartInfo prInf = new ProcessStartInfo();
            prInf.CreateNoWindow = true;
            prInf.WindowStyle = ProcessWindowStyle.Normal;
            prInf.FileName = "explorer";
            prInf.Arguments = @"/n, /select, " + path;
            pr.StartInfo = prInf;
            pr.Start();
        }

        private void btn_dtsx_path_Button_Click(object sender, RoutedEventArgs e)
        {
            Winforms.OpenFileDialog dialog = new Winforms.OpenFileDialog();
            dialog.Filter = "(*.dtsx)| *.dtsx";
            dialog.ShowDialog();
            if (string.IsNullOrEmpty(dialog.FileName))
            {
                return;
            }
            TextValues.fullPathDtsx = dialog.FileName;
            txtbox_dstx_name.Text = TextValues.fullPathDtsx.Substring(TextValues.fullPathDtsx.LastIndexOf('\\') + 1, TextValues.fullPathDtsx.Length - TextValues.fullPathDtsx.LastIndexOf('\\') - 1);
            txtbox_bat_name.Text = "DTExecBAT (" + txtbox_dstx_name.Text.Substring(0, txtbox_dstx_name.Text.LastIndexOf('.')) + ")";

            List<string> namesDtsConfigList = new List<string>();
            string configArray = "";
            int num = 0;
            string[] values = File.ReadAllLines(TextValues.fullPathDtsx);
            foreach (string val in values)
            {
                if (val.Contains(".dtsConfig") && val.Contains("ConfigurationString"))
                {
                    num++;
                    configArray += num + ". " + val.Substring(val.LastIndexOf('\\') + 1, val.Length - val.LastIndexOf('\\') - 2) + "\n";
                    namesDtsConfigList.Add(val.Substring(val.LastIndexOf('\\') + 1, val.Length - val.LastIndexOf('\\') - 2));
                }
            }

            string message;
            if (num == 0)
            {
                message = ".dtsConfig файлов в указанном .dtsx нет.";
            }
            else
            {
                if (num == 1)
                {
                    message = "Обнаружен " + num + " .dtsConfig файл:\n" + configArray;
                }
                else
                {
                    message = "Обнаружено " + num + ((num < 4) ? " .dtsConfig файла:\n" : " .dtsConfig файлов:\n") + configArray;
                }               
            }

            TextValues.namesDtsConfigList = string.Join(",", namesDtsConfigList);

            MessageBox.Show(message);

            //отобразим в путь из родительской папки расположения ExecConfig до папки расположения dtsx


            TextValues.folderDtsx = TextValues.fullPathDtsx.Substring(0, TextValues.fullPathDtsx.LastIndexOf('\\') + 1);
            string folderSturtUp = Winforms.Application.StartupPath.ToString();
            string folderMain = folderSturtUp.Substring(0, folderSturtUp.LastIndexOf('\\'));
            if (TextValues.folderDtsx.Contains(folderMain))
            {               
                string pathRalated = TextValues.folderDtsx.Replace(folderMain, "");
                TextValues.relatedPath = pathRalated;
            }
            else
            {
                string errorMessage = "ExecConfig.exe расположен не в одной из родительских папок " + txtbox_dstx_name.Text;
                txtbox_dstx_name.Text = "";
                MessageBox.Show(errorMessage);
            }

        }
    }

    public class TextValues
    {
        // полный путь к файлу .dtsx
        public static string fullPathDtsx = "";
        public static string namesDtsConfigList { get; set; }
        public static string namesDtsConfigListMissing { get; set; }
        public static string relatedPath = "";
        public static string folderDtsx = "";

    }
}
